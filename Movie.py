import numpy as np
import matplotlib.pyplot as plt
from EntranceRamp import *
from Highway import *
from MileMarker import *
from Car import *

h1 = Highway(100, ifdeer = True, frac = 0.5, ncars = 10, deerchance = 0.05)
#print h1.getCarPositions()
#h1.advanceTime()
#print h1.getCarPositions()



for i in range(200):
    h1.advanceTime()
    carnumbers = h1.getCarNumbers()
#    print carnumbers
    carpos = h1.getCarPositions()
    string1 =  "Car number " + str(np.min(carnumbers)) + " is the furthest along. It is " + str(100 - np.max(carpos)) + " meters away from the end"
    string2 =  "Car number " + str(np.max(carnumbers)) + " is the furthest away. It is " + str(100 - np.min(carpos)) + " meters away from the end"
    y = np.zeros(len(h1.getCarPositions()))
#    print y
    x1 = h1.getCarPositions()
    x = x1 + np.zeros(len(h1.getCarPositions()))
    deer = h1.getDeerLoc()
    y1 = np.zeros(len(h1.getDeerLoc()))
    y1 = 0.1 + np.zeros(len(h1.getDeerLoc()))
    t = np.linspace(0, 2 * np.pi, 20)
    color = np.cos(t)
    plt.scatter(x, y, c='blue', alpha = 0.7, label = 'Cars')
    plt.scatter(deer, y1, c='orange', label = "Deer")
    plt.title("Highway at Time " + str(i))
    plt.xlim(0, 101)
    plt.ylim(-1, 1)
    plt.text(2, -0.5, string1)
    plt.text(2, -0.6, string2)
    plt.legend(loc = 'upper right', numpoints = 1)
    plt.savefig('movieimages/image'+str(i)+'.png')
    plt.clf()
#    plt.show()
#    print h1.getCarNumbers()

