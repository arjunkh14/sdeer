import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
from scipy import integrate
from math import log

def quadratic(x, a, b, c):
    return a*(x**2) + b*x + c

def lse(x, y):
    sumx = np.sum(x)
    sumy = np.sum(y)
    sumxy = np.sum(x*y)
    sumx2 = np.sum(x*x)
    a1 = ((sumy)*(sumx2) - (sumx)*(sumxy))/((sumx2) - ((sumx)**2))
#    a2 = (sumxy - ((sumy)*(sumx)))/(sumx2 - ((sumx)**2))
    a2 = np.mean(y) - (a1 * np.mean(x))
    return a1, a2

def linear(x, m, c):
    return m*x + c

def logf(x, a, b, c):
#    return a * log(x, c) + b
    return a * (np.log(x)/np.log(c)) + b
#    return a * np.log(x) + b

def asymp_quad(x, a, b, c, p, q, r):
    return (a * (x**2) + b*x + c)/(p*(x**2) + q*x + r)

deerprob, mean_deer, sd_deer = np.loadtxt('probdeer.txt', usecols = (0, 1, 3), unpack = True)
slowdown, mean_slowdown, sd_slowdown = np.loadtxt('slowdown.txt', usecols = (0, 1, 3), unpack = True)
react, mean_react, sd_react = np.loadtxt('carsreact.txt', usecols = (0, 1, 3), unpack = True)


#Doing Slowdown

p0 = [0, 3500]
params1, pcov1 = curve_fit(linear, slowdown, mean_slowdown, p0 = p0)
print "parameters (a, b, c) = ", str(params1)
print "covariance matrix = ", str(pcov1)

p11, p12 = params1[0], params1[1]

prob_new = np.linspace(np.min(slowdown), np.max(slowdown), 20)
fit = linear(prob_new, p11, p12)

#b1, b2 = lse(slowdown, mean_slowdown)
#print "slope = ", str(b1), "intercept = ", str(b2)

#xaxis = np.linspace(np.min(slowdown), np.max(slowdown), 100)
#fit = b1*xaxis + b2

string_slowdown = "Fitted Function = " + str(round(p11, 2)) + "*x + " + str(round(p12, 2))

plt.errorbar(slowdown, mean_slowdown, yerr = sd_slowdown, fmt = 'o')
plt.plot(prob_new, fit, c = 'red', label = 'Least Squares Regression')
plt.ylim(0, np.max(mean_slowdown) + 1000)
plt.xlabel('Fraction Cars Slow Down to After Seeing Deer')
plt.ylabel('Time Steps until Completion')
plt.title('Time Steps vs Fraction Slow Down (ncars = 10, prob = 0.06)')
plt.text(0.50, 3900, string_slowdown)
plt.legend(loc = 'upper right')
plt.savefig('fit_slowdown.png')
plt.show()
plt.clf()





#Now doing Deer Probability
p0 = [10, 10, 10]
plog = [1, 1, 70]
parlog, covlog = curve_fit(logf, deerprob, mean_deer, p0 = plog)
print "parameters (a, b, c) = ", str(parlog)
print "covariance matrix = ", str(covlog)
plog1, plog2, plog3 = parlog[0], parlog[1], parlog[2]
prob_log = np.linspace(np.min(deerprob), np.max(deerprob), 20)
fitlog = logf(prob_log, plog1, plog2, plog3)

params2, pcov2 = curve_fit(quadratic, deerprob, mean_deer, p0 = p0)
print "parameters (a, b, c) = ", str(params2)
print "covariance matrix = ", str(pcov2)
p21, p22, p23 = params2[0], params2[1], params2[2]
prob_new = np.linspace(np.min(deerprob), np.max(deerprob), 20)
fit1 = quadratic(prob_new, p21, p22, p23)

pasymp_quad = [1, 1, 1, 1, 1, 1]
params3, pcov3 = curve_fit(asymp_quad, deerprob, mean_deer, p0 = pasymp_quad)

print "parameters (a, b, c, p, q, r) = ", str(params3)
print "covariance matrix = ", str(pcov3)

p31, p32, p33, p34, p35, p36 = params3[0], params3[1], params3[2], params3[3], params3[4], params3[5]

prob_asympquad = np.linspace(np.min(deerprob), np.max(deerprob), 20)
fit2 = asymp_quad(prob_asympquad, p31, p32, p33, p34, p35, p36)

string_prob = "Fitted Function = (" + str(round(p31, 1)) + "x**2 + " + str(round(p32, 1)) + "x + " +str(round(p33, 1)) + ")/(" + str(round(p34, 2)) + "x**2 + " + str(round(p35, 2)) + "x + " + str(round(p36, 1)) + ")"


plt.errorbar(deerprob, mean_deer, yerr = sd_deer, fmt = 'o', label = 'data point')
#plt.plot(prob_new, fit1, c = 'red', label = 'fitted function')
#plt.plot(prob_log, fitlog, c = 'green', label = 'log function a * log(x) + b')
plt.plot(prob_asympquad, fit2, c = 'red', label = 'asymptotic quadratic')
plt.ylim(0, np.max(mean_deer) + 2500)
plt.xlim(0, 0.5)
plt.xlabel('Probability of Deer at any Point')
plt.ylabel('Time Steps until Completion')
plt.title('Time Steps vs Deer Probability (frac = 0.5, ncars = 10)')
plt.text(0.03, 6700, string_prob, size = 'small')
plt.legend(loc = 'top right', numpoints = 1)
plt.savefig('fit_prob.png')
plt.show()
plt.clf()



#Now doing Cars Reacting

pasymp_quad = [1, 1, 1, 1, 1, 1]
params3, pcov3 = curve_fit(asymp_quad, react, mean_react, p0 = pasymp_quad)

print "parameters (a, b, c, p, q, r) = ", str(params3)
print "covariance matrix = ", str(pcov3)

p31, p32, p33, p34, p35, p36 = params3[0], params3[1], params3[2], params3[3], params3[4], params3[5]

react_new = np.linspace(np.min(react), np.max(react), 20)
fit2 = asymp_quad(react_new, p31, p32, p33, p34, p35, p36)

string_react = "Fitted Function = (" + str(round(p31, 1)) + "x**2 + " + str(round(p32, 1)) + "x + " +str(round(p33, 1)) + ")/(" + str(round(p34, 2)) + "x**2 + " + str(round(p35, 2)) + "x + " + str(round(p36, 1)) + ")"

plt.errorbar(react, mean_react, yerr = sd_react, fmt = 'o', label = 'data point')
plt.plot(react_new, fit2, c = 'red', label = 'fitted function')
plt.ylim(0, np.max(mean_react) + 2000)
plt.xlabel('Number of Cars Slowing Down after a Wild Deer Appears')
plt.ylabel('Time Steps until Completion')
plt.title('Time Steps vs Cars Slowing Down (frac = 0.5, prob = 0.06)')
plt.text(10, 5700, string_react, size = 'small')
plt.legend(loc = 'upper right', numpoints = 1)
plt.savefig('fit_react.png')
plt.show()
plt.clf()
#'''




