import numpy as np
import matplotlib.pyplot as plt

class MileMarker():
    def __init__(self, miles_since_start, probability):
        self.miles_since_start = miles_since_start
        self.probability = probability

    def deerOccurrence(self):
        rando = np.random.random()
        if rando <= self.probability:
            return True
        else:
            return False

    def getMiles(self):
        return self.miles_since_start


