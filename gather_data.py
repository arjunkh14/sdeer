import numpy as np
import matplotlib.pyplot as plt
from MileMarker import *
from Car import *
from EntranceRamp import *
from Highway import *
from datetime import datetime
from datetime import timedelta

def run_without_deer():
    h = Highway(1000,False,.5, 10, .20)
    n = h.numCarExited
    t = 0
    while n() != 1000 and t < 1102:
        h.advanceTime()
        t+=1
        print 'Time = %d' % t
        
        #print h.getCarPositions()
        print h.numCarExited()
        #print h.numCarOnRoad()
        #print h.numCarWaiting()

def run_with_deer(frac = .5, prob=.20, ncars=10):
    h = Highway(1000,True, frac, ncars, prob)
    n = h.numCarExited
    t = 0
    while n() != 1000:
        h.advanceTime()
        t+=1
    return t

t1 =datetime.now()
fracs = []
t = []

avg = []
meds = []
stds = []

l = [1,2,4,6,8,10,15,20,25,30,35,40, 45, 50]
# run data for varying the fraction
for i in l:
    print i
    fracs.append(i)
    inter = []
    for elem in range(100):
        inter.append(run_with_deer(frac = .5, prob=.10, ncars=i))
    avg.append(np.mean(inter))
    meds.append(np.median(inter))
    stds.append(np.std(inter))
data = np.zeros((len(l),4))
data[:,0] = l
data[:,1] = avg
data[:,2] = meds
data[:,3] = stds
np.savetxt('varying_cars_to_react.txt', data, header = 'Number of cars to slow down, Average time for all cars to exit, Median Time for all cars to exit, standard error')
print 'Done varying number of cars to react'
avg = []
meds = []
stds = []
l = np.linspace(0.1,.95,18)
print l
# run data for varying the fraction
for i in l:
    print i
    fracs.append(i)
    inter = []
    for elem in range(100):
        inter.append(run_with_deer(frac = i, prob=.10, ncars=10))
    avg.append(np.mean(inter))
    meds.append(np.median(inter))
    stds.append(np.std(inter))
data = np.zeros((len(l),4))
data[:,0] = l
data[:,1] = avg
data[:,2] = meds
data[:,3] = stds
np.savetxt('varying_factor_by_which_car_speed_changes.txt', data, header = 'factor by which car slows down, Average time for all cars to exit, Median Time for all cars to exit, standard error')
print "Done varying the factor by which the car's speed changes."

avg = []
meds = []
stds = []

l = [0.01,0.02,0.03,0.04,0.05,0.06,0.07,0.08,0.09,0.10,0.15,0.20,0.25,0.30,0.35,0.40]

# run data for varying the fraction
for i in l:
    print i
    fracs.append(i)
    inter = []
    for elem in range(100):
        inter.append(run_with_deer(frac = 0.5, prob=i, ncars=10))
    avg.append(np.mean(inter))
    meds.append(np.median(inter))
    stds.append(np.std(inter))
data = np.zeros((len(l),4))
data[:,0] = l
data[:,1] = avg
data[:,2] = meds
data[:,3] = stds
np.savetxt('varying_prob_of_deer.txt', data, header = 'prob of deer, Average time for all cars to exit, Median Time for all cars to exit, standard error')
t2 =datetime.now()
print t2-t1
print "done"
