import numpy as np
import matplotlib.pyplot as plt
from EntranceRamp import *
from Highway import *
from MileMarker import *
from Car import *

def run_until_end(frac, ncars, deerchance):
    h = Highway(1000, ifdeer = True, frac = frac, ncars = ncars, deerchance = deerchance)
    t = 0
    n = h.numCarExited
    while h.numCarExited() != 1000:
        h.advanceTime()
        t += 1
    return t

def deerprob(frac, ncars, deerprob): #This function loops over deerprob
    mean = []
    sd = []
    for j in deerprob:
        times = []
        print "Probability of deer = ", str(j)
        for i in range(10):
            t = run_until_end(frac, ncars, j)
            times.append(t)
#            print t
#        print "times = ", str(times)
        mean.append(np.mean(times))
        sd.append(np.std(times))
        print "mean = ", str(mean[-1])
        print "sd = ", str(sd[-1])
    return mean, sd

def frac(frac, ncars, deerprob): #This function loops over frac
    mean = []
    sd = []
    for j in frac:
        times = []
        print "Fraction slowing down = ", str(j)
        for i in range(10):
            t = run_until_end(j, ncars, deerprob)
            times.append(t)
#            print t
#        print "times = ", str(times)
        mean.append(np.mean(times))
        sd.append(np.std(times))
        print "mean = ", str(mean[-1])
        print "sd = ", str(sd[-1])
        print ""
    return mean, sd


def ncars(frac, ncars, deerprob): #This function loops over ncars
    mean = []
    sd = []
    for j in ncars:
        times = []
        print "Cars reacting = ", str(j)
        for i in range(10):
            t = run_until_end(frac, j, deerprob)
            times.append(t)
#            print t
#        print "times = ", str(times)
        mean.append(np.mean(times))
        sd.append(np.std(times))
        print "mean = ", str(mean[-1])
        print "sd = ", str(sd[-1])
        print ""
    return mean, sd





#Now we define the arrays over which we'll loop the paramters over

slowdown = [0.01]
for i in range(5, 100, 5):
    slowdown.append(i*0.01)

carsreact = [1, 5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95]

#probdeer = [0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2, 0.22, 0.24, 0.26, 0.28, 0.3, 0.32, 0.34, 0.36, 0.38, 0.4, 0.5, 0.6, 0.7, 0.8, 0.85, 0.90, 0.95, 0.99]
probdeer = [0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2, 0.22, 0.24, 0.26, 0.28, 0.3, 0.32, 0.34, 0.36, 0.38, 0.4]
#probdeer = [0.02, 0.04, 0.06, 0.08, 0.1, 0.12, 0.14, 0.16, 0.18, 0.2]

#Using mini arrays to speed up the bug fixing
#probdeer = [0.02, 0.04, 0.06, 0.08, 0.10]
#slowdown = [0.1, 0.3, 0.5, 0.7, 0.9]
#carsreact = [5, 10, 20, 40, 80]

from datetime import datetime
t1 = datetime.now()


#Now we plot Sim time vs Deer prob, holding ncars constant
print "WE'RE ON THE FIRST OF SIX PLOTS"


print "Now doing Frac = 0.01"
print ""
m_dc_frac01_n10, sd_dc_frac01_n10 = deerprob(0.01, 10, probdeer)
print "Now doing Frac = 0.05"
print ""
m_dc_frac05_n10, sd_dc_frac05_n10 = deerprob(0.05, 10, probdeer)
print "Now doing Frac = 0.10"
print ""
m_dc_frac10_n10, sd_dc_frac10_n10 = deerprob(0.10, 10, probdeer)
print "Now doing Frac = 0.50"
print ""
m_dc_frac50_n10, sd_dc_frac50_n10 = deerprob(0.5, 10, probdeer)
print "Now doing Frac = 0.80"
print ""
m_dc_frac80_n10, sd_dc_frac80_n10 = deerprob(0.8, 10, probdeer)

max01 = np.max(m_dc_frac01_n10)
max05 = np.max(m_dc_frac05_n10)
max10 = np.max(m_dc_frac10_n10)
max50 = np.max(m_dc_frac50_n10)
max80 = np.max(m_dc_frac80_n10)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(probdeer, m_dc_frac01_n10, yerr = sd_dc_frac01_n10, fmt = '--o', c = 'red', ecolor = 'black', label = "Fraction = 0.01")
plt.errorbar(probdeer, m_dc_frac05_n10, yerr = sd_dc_frac05_n10, fmt = '--o', c = 'orange', ecolor = 'black', label = "Fraction = 0.05")
plt.errorbar(probdeer, m_dc_frac10_n10, yerr = sd_dc_frac10_n10, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Fraction = 0.10")
plt.errorbar(probdeer, m_dc_frac50_n10, yerr = sd_dc_frac50_n10, fmt = '--o', c = 'green', ecolor = 'black', label = "Fraction = 0.50")
plt.errorbar(probdeer, m_dc_frac80_n10, yerr = sd_dc_frac80_n10, fmt = '--o', c = 'blue', ecolor = 'black', label = "Fraction = 0.80")
plt.ylim(0, max + 500)
plt.xlim(0, 0.50)
plt.title('Time Steps vs Deer Probability (ncars = 10)')
plt.xlabel('Deer Probability')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('Deerprob-holdncars.png')
#plt.show()
data = np.zeros((len(probdeer), 11))
data[:,0] = probdeer
data[:,1] = m_dc_frac01_n10
data[:,2] = sd_dc_frac01_n10
data[:,3] = m_dc_frac05_n10
data[:,4] = sd_dc_frac05_n10
data[:,5] = m_dc_frac10_n10
data[:,6] = sd_dc_frac10_n10
data[:,7] = m_dc_frac50_n10
data[:,8] = sd_dc_frac50_n10
data[:,9] = m_dc_frac80_n10
data[:,10] = sd_dc_frac80_n10
np.savetxt('probdeer-holdncars.txt', data) 
plt.clf()



#Now we plot Sim time vs Deer prob, holding frac constant

print "WE'RE ON THE SECOND OF SIX PLOTS"

print "Now doing ncars = 05"
print ""
m_dc_frac50_n05, sd_dc_frac50_n05 = deerprob(0.5, 5, probdeer)
print "Now doing ncars = 10"
print ""
m_dc_frac50_n10, sd_dc_frac50_n10 = deerprob(0.5, 10, probdeer)
print "Now doing ncars = 20"
print ""
m_dc_frac50_n20, sd_dc_frac50_n20 = deerprob(0.5, 20, probdeer)
print "Now doing ncars = 40"
print ""
m_dc_frac50_n40, sd_dc_frac50_n40 = deerprob(0.5, 40, probdeer)
print "Now doing ncars = 80"
print ""
m_dc_frac50_n80, sd_dc_frac50_n80 = deerprob(0.5, 80, probdeer)

max01 = np.max(m_dc_frac50_n05)
max05 = np.max(m_dc_frac50_n10)
max10 = np.max(m_dc_frac50_n20)
max50 = np.max(m_dc_frac50_n40)
max80 = np.max(m_dc_frac50_n80)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(probdeer, m_dc_frac50_n80, yerr = sd_dc_frac50_n80, fmt = '--o', c = 'red', ecolor = 'black', label = "Ncars = 80")
plt.errorbar(probdeer, m_dc_frac50_n40, yerr = sd_dc_frac50_n40, fmt = '--o', c = 'orange', ecolor = 'black', label = "Ncars = 40")
plt.errorbar(probdeer, m_dc_frac50_n20, yerr = sd_dc_frac50_n20, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Ncars = 20")
plt.errorbar(probdeer, m_dc_frac50_n10, yerr = sd_dc_frac50_n10, fmt = '--o', c = 'green', ecolor = 'black', label = "Ncars = 10")
plt.errorbar(probdeer, m_dc_frac50_n05, yerr = sd_dc_frac50_n05, fmt = '--o', c = 'blue', ecolor = 'black', label = "Ncars = 5")
plt.ylim(0, max + 500)
plt.xlim(0, 0.50)
plt.title('Time Steps vs Deer Probability (frac = 0.5)')
plt.xlabel('Deer Probability')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('Deerprob-holdfrac.png')
#plt.show()
data = np.zeros((len(probdeer), 11))
data[:,0] = probdeer
data[:,1] = m_dc_frac50_n80
data[:,2] = sd_dc_frac50_n80
data[:,3] = m_dc_frac50_n40
data[:,4] = sd_dc_frac50_n40
data[:,5] = m_dc_frac50_n20
data[:,6] = sd_dc_frac50_n20
data[:,7] = m_dc_frac50_n10
data[:,8] = sd_dc_frac50_n10
data[:,9] = m_dc_frac50_n05
data[:,10] = sd_dc_frac50_n05
np.savetxt('probdeer-holdfrac.txt', data) 
plt.clf()



#Now we plot Sim time vs Frac, holding ncars constant

print "WE'RE ON THE THIRD OF SIX PLOTS"


print "Now doing Prob = 0.02"
print ""
m_frac_dc02_n10, sd_frac_dc02_n10 = frac(slowdown, 10, 0.02)
print "Now doing Prob = 0.06"
print ""
m_frac_dc06_n10, sd_frac_dc06_n10 = frac(slowdown, 10, 0.06)
print "Now doing Prob = 0.16"
print ""
m_frac_dc16_n10, sd_frac_dc16_n10 = frac(slowdown, 10, 0.16)
print "Now doing Prob = 0.30"
print ""
m_frac_dc30_n10, sd_frac_dc30_n10 = frac(slowdown, 10, 0.30)
print "Now doing Prob = 0.40"
print ""
m_frac_dc40_n10, sd_frac_dc40_n10 = frac(slowdown, 10, 0.40)

max01 = np.max(m_frac_dc02_n10)
max05 = np.max(m_frac_dc06_n10)
max10 = np.max(m_frac_dc16_n10)
max50 = np.max(m_frac_dc30_n10)
max80 = np.max(m_frac_dc40_n10)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(slowdown, m_frac_dc40_n10, yerr = sd_frac_dc40_n10, fmt = '--o', c = 'red', ecolor = 'black', label = "Probability = 0.40")
plt.errorbar(slowdown, m_frac_dc30_n10, yerr = sd_frac_dc30_n10, fmt = '--o', c = 'orange', ecolor = 'black', label = "Probability = 0.30")
plt.errorbar(slowdown, m_frac_dc16_n10, yerr = sd_frac_dc16_n10, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Probability = 0.16")
plt.errorbar(slowdown, m_frac_dc06_n10, yerr = sd_frac_dc06_n10, fmt = '--o', c = 'green', ecolor = 'black', label = "Probability = 0.06")
plt.errorbar(slowdown, m_frac_dc02_n10, yerr = sd_frac_dc02_n10, fmt = '--o', c = 'blue', ecolor = 'black', label = "Probability = 0.02")
plt.ylim(0, max + 500)
plt.xlim(0, 1)
plt.title('Time Steps vs Slowdown Fraction (ncars = 10)')
plt.xlabel('Slowdown Fraction after seeing Deer')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('Slowdown-holdncars.png')
#plt.show()
data = np.zeros((len(slowdown), 11))
data[:,0] = probdeer
data[:,1] = m_frac_dc40_n10
data[:,2] = sd_frac_dc40_n10
data[:,3] = m_frac_dc30_n10
data[:,4] = sd_frac_dc30_n10
data[:,5] = m_frac_dc16_n10
data[:,6] = sd_frac_dc16_n10
data[:,7] = m_frac_dc06_n10
data[:,8] = sd_frac_dc06_n10
data[:,9] = m_frac_dc02_n10
data[:,10] = sd_frac_dc02_n10
np.savetxt('Slowdown-holdncars.txt', data) 
plt.clf()


#Now we plot Sim time vs Frac, holding deer probability constant

print "WE'RE ON THE FOURTH OF SIX PLOTS"


print "Now doing ncars = 5"
print ""
m_frac_dc06_n05, sd_frac_dc06_n05 = frac(slowdown, 5, 0.06)
print "Now doing ncars = 10"
print ""
m_frac_dc06_n10, sd_frac_dc06_n10 = frac(slowdown, 10, 0.06)
print "Now doing ncars = 20"
print ""
m_frac_dc06_n20, sd_frac_dc06_n20 = frac(slowdown, 20, 0.06)
print "Now doing ncars = 40"
print ""
m_frac_dc06_n40, sd_frac_dc06_n40 = frac(slowdown, 40, 0.06)
print "Now doing ncars = 80"
print ""
m_frac_dc06_n80, sd_frac_dc06_n80 = frac(slowdown, 80, 0.06)

max01 = np.max(m_frac_dc06_n05)
max05 = np.max(m_frac_dc06_n10)
max10 = np.max(m_frac_dc06_n20)
max50 = np.max(m_frac_dc06_n40)
max80 = np.max(m_frac_dc06_n80)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(slowdown, m_frac_dc06_n80, yerr = sd_frac_dc06_n80, fmt = '--o', c = 'red', ecolor = 'black', label = "Ncars = 80")
plt.errorbar(slowdown, m_frac_dc06_n40, yerr = sd_frac_dc06_n40, fmt = '--o', c = 'orange', ecolor = 'black', label = "Ncars = 40")
plt.errorbar(slowdown, m_frac_dc06_n20, yerr = sd_frac_dc06_n20, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Ncars = 20")
plt.errorbar(slowdown, m_frac_dc06_n10, yerr = sd_frac_dc06_n10, fmt = '--o', c = 'green', ecolor = 'black', label = "Ncars = 10")
plt.errorbar(slowdown, m_frac_dc06_n05, yerr = sd_frac_dc06_n05, fmt = '--o', c = 'blue', ecolor = 'black', label = "Ncars = 5")
plt.ylim(0, max + 500)
plt.xlim(0, 1)
plt.title('Time Steps vs Slowdown Fraction (deer prob = 0.06)')
plt.xlabel('Slowdown Fraction after seeing Deer')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('Slowdown-holdndeerprob.png')
#plt.show()
data = np.zeros((len(slowdown), 11))
data[:,0] = probdeer
data[:,1] = m_frac_dc06_n80
data[:,2] = sd_frac_dc06_n80
data[:,3] = m_frac_dc06_n40
data[:,4] = sd_frac_dc06_n40
data[:,5] = m_frac_dc06_n20
data[:,6] = sd_frac_dc06_n20
data[:,7] = m_frac_dc06_n10
data[:,8] = sd_frac_dc06_n10
data[:,9] = m_frac_dc06_n05
data[:,10] = sd_frac_dc06_n05
np.savetxt('Slowdown-holdncars.txt', data) 
plt.clf()



#Now we plot Sim time vs Ncars, holding deer prob constant

print "WE'RE ON THE FIFTH OF SIX PLOTS"

print "Now doing frac = 0.01"
print ""
m_dc06_frac01_n, sd_dc06_frac01_n = ncars(0.01, carsreact, 0.06)
print "Now doing frac = 0.05"
print ""
m_dc06_frac05_n, sd_dc06_frac05_n = ncars(0.05, carsreact, 0.06)
print "Now doing frac = 0.10"
print ""
m_dc06_frac10_n, sd_dc06_frac10_n = ncars(0.1, carsreact, 0.06)
print "Now doing frac = 0.50"
print ""
m_dc06_frac50_n, sd_dc06_frac50_n = ncars(0.5, carsreact, 0.06)
print "Now doing frac = 0.80"
print ""
m_dc06_frac80_n, sd_dc06_frac80_n = ncars(0.8, carsreact, 0.06)

max01 = np.max(m_dc06_frac01_n)
max05 = np.max(m_dc06_frac05_n)
max10 = np.max(m_dc06_frac10_n)
max50 = np.max(m_dc06_frac50_n)
max80 = np.max(m_dc06_frac80_n)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(carsreact, m_dc06_frac01_n, yerr = sd_dc06_frac01_n, fmt = '--o', c = 'red', ecolor = 'black', label = "Frac = 0.01")
plt.errorbar(carsreact, m_dc06_frac05_n, yerr = sd_dc06_frac05_n, fmt = '--o', c = 'orange', ecolor = 'black', label = "Frac = 0.05")
plt.errorbar(carsreact, m_dc06_frac10_n, yerr = sd_dc06_frac10_n, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Frac = 0.10")
plt.errorbar(carsreact, m_dc06_frac50_n, yerr = sd_dc06_frac50_n, fmt = '--o', c = 'green', ecolor = 'black', label = "Frac = 0.50")
plt.errorbar(carsreact, m_dc06_frac80_n, yerr = sd_dc06_frac80_n, fmt = '--o', c = 'blue', ecolor = 'black', label = "Frac = 0.80")
plt.ylim(0, max + 500)
plt.xlim(0, 100)
plt.title('Time Steps vs Cars Reacting (deer prob = 0.06)')
plt.xlabel('Number of Cars Reacting to Deer')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('ncars-holddeerprob.png')
#plt.show()
data = np.zeros((len(carsreact), 11))
data[:,0] = carsreact
data[:,1] = m_dc06_frac01_n
data[:,2] = sd_dc06_frac01_n
data[:,3] = m_dc06_frac05_n
data[:,4] = sd_dc06_frac05_n
data[:,5] = m_dc06_frac10_n
data[:,6] = sd_dc06_frac10_n
data[:,7] = m_dc06_frac50_n
data[:,8] = sd_dc06_frac50_n
data[:,9] = m_dc06_frac80_n
data[:,10] = sd_dc06_frac80_n
np.savetxt('ncars-holddeerprob.txt', data) 
plt.clf()


#Now we plot Sim time vs Ncars, holding frac constant

print "WE'RE ON THE SIXTH OF SIX PLOTS"


print "Now doing deer prob = 0.02"
print ""
m_dc02_frac50_n, sd_dc02_frac50_n = ncars(0.50, carsreact, 0.02)
print "Now doing deer prob = 0.06"
print ""
m_dc06_frac50_n, sd_dc06_frac50_n = ncars(0.50, carsreact, 0.06)
print "Now doing deer prob = 0.16"
print ""
m_dc16_frac50_n, sd_dc16_frac50_n = ncars(0.50, carsreact, 0.16)
print "Now doing deer prob = 0.30"
print ""
m_dc30_frac50_n, sd_dc30_frac50_n = ncars(0.5, carsreact, 0.30)
print "Now doing deer prob = 0.40"
print ""
m_dc40_frac50_n, sd_dc40_frac50_n = ncars(0.8, carsreact, 0.40)

max01 = np.max(m_dc02_frac50_n)
max05 = np.max(m_dc06_frac50_n)
max10 = np.max(m_dc16_frac50_n)
max50 = np.max(m_dc30_frac50_n)
max80 = np.max(m_dc40_frac50_n)
max = np.max([max01, max05, max10, max50, max80])

plt.errorbar(carsreact, m_dc40_frac50_n, yerr = sd_dc40_frac50_n, fmt = '--o', c = 'red', ecolor = 'black', label = "Prob = 0.40")
plt.errorbar(carsreact, m_dc30_frac50_n, yerr = sd_dc30_frac50_n, fmt = '--o', c = 'orange', ecolor = 'black', label = "Prob = 0.30")
plt.errorbar(carsreact, m_dc16_frac50_n, yerr = sd_dc16_frac50_n, fmt = '--o', c = 'yellow', ecolor = 'black', label = "Prob = 0.16")
plt.errorbar(carsreact, m_dc06_frac50_n, yerr = sd_dc06_frac50_n, fmt = '--o', c = 'green', ecolor = 'black', label = "Prob = 0.06")
plt.errorbar(carsreact, m_dc02_frac50_n, yerr = sd_dc02_frac50_n, fmt = '--o', c = 'blue', ecolor = 'black', label = "Prob = 0.02")
plt.ylim(0, max + 500)
plt.xlim(0, 100)
plt.title('Time Steps vs Cars Reacting (frac = 0.50)')
plt.xlabel('Number of Cars Reacting to Deer')
plt.ylabel('Time Steps to Completion')
plt.legend(loc = 'lower right', numpoints = 1)
plt.savefig('ncars-holdfrac.png')
#plt.show()
data = np.zeros((len(carsreact), 11))
data[:,0] = carsreact
data[:,1] = m_dc40_frac50_n
data[:,2] = sd_dc40_frac50_n
data[:,3] = m_dc30_frac50_n
data[:,4] = sd_dc30_frac50_n
data[:,5] = m_dc16_frac50_n
data[:,6] = sd_dc16_frac50_n
data[:,7] = m_dc06_frac50_n
data[:,8] = sd_dc06_frac50_n
data[:,9] = m_dc02_frac50_n
data[:,10] = sd_dc02_frac50_n
np.savetxt('ncars-holdfrac.txt', data) 
plt.clf()



t2 = datetime.now()

print "Total time = ", str(t2 - t1)









'''

for i in carsreact:
    array1 = []
    print "number of cars reacting = ", str(i)
    for j in range(10):
        t = run_until_end(0.5, i, 0.2)
        j += 1
        array1.append(t)
    mean_react.append(np.mean(array1))
    median_react.append(np.median(array1))
    sd_react.append(np.std(array1))
    print "mean = ", str(mean_react[-1])
    print "median = ", str(median_react[-1]) 
    print "standard deviation = ", str(sd_react[-1])
    print ""

print "mean (react) = ", str(mean_react)
print "median (react) = ", str(median_react)
print "standard deviation (react) = ", str(sd_react)

plt.errorbar(carsreact, mean_react, yerr = sd_react, fmt = '--o', c = 'blue', ecolor = 'black')
plt.ylim(0, np.max(mean_react) + 200)
plt.title('Time Steps to Completion vs Number of Cars Reacting (frac = 0.5, deerchance = 0.2)')
plt.xlabel('Number of Cars Reacting to Deer')
plt.ylabel('Time steps until completion')
plt.savefig('Time-vs-carsreact.png')
plt.show()

'''




#Now I define arrays which will store the different values of frac, ncars, and deerchance



#I define empty arrays that will store the number of steps, means, medians, and standard deviations of the three different parameters
steps_slowdown = []
steps_react = []
steps_deer = []

mean_slowdown = []
mean_react = []
mean_deer = []

median_slowdown = []
median_react = []
median_deer = []

sd_slowdown = []
sd_react = []
sd_deer = []
'''

#Running the function for various values of ncars
for i in carsreact:
    array1 = []
    print "number of cars reacting = ", str(i)
    for j in range(10):
        t = run_until_end(0.5, i, 0.06)
        j += 1
        array1.append(t)
    mean_react.append(np.mean(array1))
    median_react.append(np.median(array1))
    sd_react.append(np.std(array1))
    print "mean = ", str(mean_react[-1])
    print "median = ", str(median_react[-1]) 
    print "standard deviation = ", str(sd_react[-1])
    print ""

print "mean (react) = ", str(mean_react)
print "median (react) = ", str(median_react)
print "standard deviation (react) = ", str(sd_react)

plt.errorbar(carsreact, mean_react, yerr = sd_react, fmt = '--o', c = 'blue', ecolor = 'black')
plt.ylim(0, np.max(mean_react) + 200)
plt.title('Time Steps vs Cars Reacting (frac = 0.5, deerchance = 0.06)')
plt.xlabel('Number of Cars Reacting to Deer')
plt.ylabel('Time steps until completion')
plt.savefig('Time-vs-carsreact.png')
plt.show()
data = np.zeros((len(carsreact), 4))
data[:,0] = carsreact
data[:,1] = mean_react
data[:,2] = median_react
data[:,3] = sd_react
np.savetxt('carsreact.txt', data) 






#Running the function for various values of frac
for i in slowdown:
    array1 = []
    print "fraction of speed slowing down to = ", str(i)
    for j in range(10):
        t = run_until_end(i, 10, 0.06)
        j += 1
        array1.append(t)
    mean_slowdown.append(np.mean(array1))
    median_slowdown.append(np.median(array1))
    sd_slowdown.append(np.std(array1))
    print "mean = ", str(mean_slowdown[-1])
    print "median = ", str(median_slowdown[-1]) 
    print "standard deviation = ", str(sd_slowdown[-1])
    print ""

#plt.scatter(slowdown, mean_slowdown, c = 'blue')
fig = plt.figure(figsize = (8, 8))
ax = fig.add_subplot(111)
ax.errorbar(slowdown, mean_slowdown, yerr = sd_slowdown, fmt = '--o', c = 'blue', ecolor = 'black')
#plt.scatter(slowdown, median_slowdown, c= 'green')
ax.set_ylim(0, np.max(mean_slowdown) + 200)
ax.set_xlim(0, 1.0)
ax.set_xlabel('Fraction of speed cars slow down to after seeing deer')
ax.set_ylabel('Time steps until completion')
ax.set_title('Time Steps vs Fraction of Speed Cars Slow To (ncars = 10, deerchance = 0.06)')
plt.savefig('Time-vs-slowdownfrac.png')
plt.show()
data = np.zeros((len(slowdown), 4))
data[:,0] = slowdown
data[:,1] = mean_slowdown
data[:,2] = median_slowdown
data[:,3] = sd_slowdown
np.savetxt('slowdown.txt', data) 



#Running the function for various values of deerchance
for i in probdeer:
    array1 = []
    print "prob of deer = ", str(i)
    for j in range(10):
        t = run_until_end(0.5, 10, i)
        j += 1
        array1.append(t)
    mean_deer.append(np.mean(array1))
    median_deer.append(np.median(array1))
    sd_deer.append(np.std(array1))
    print "mean = ", str(mean_deer[-1])
    print "median = ", str(median_deer[-1]) 
    print "standard deviation = ", str(sd_deer[-1])
    print ""

plt.errorbar(probdeer, mean_deer, yerr = sd_deer, fmt = '--o', c = 'blue', ecolor = 'black')
plt.ylim(0, np.max(mean_deer) + 200)
plt.title('Time Steps vs Prob of Deer (frac = 0.5, ncars = 10)')
plt.xlabel('Probability of Deer Appearing at any Place')
plt.ylabel('Time Steps until Completion')
plt.savefig('Time-vs-deerprob.png')
plt.show()
data = np.zeros((len(probdeer), 4))
data[:,0] = probdeer
data[:,1] = mean_deer
data[:,2] = median_deer
data[:,3] = sd_deer
np.savetxt('probdeer.txt', data) 

'''




'''
#Running the function for all values of all parameters
for i in probdeer:
    for j in carsreact:
        for k in slowdown:
            array1 = []
            print "slowdown fraction = ", str(k)
            for j in range(10):
                t = run_until_end(k, j, i)
                j += 1
                array1.append(t)
            mean_deer.append(np.mean(array1))
            median_deer.append(np.median(array1))
            sd_deer.append(np.std(array1))
    print "mean = ", str(mean_deer[-1])
    print "median = ", str(median_deer[-1]) 
    print "standard deviation = ", str(sd_deer[-1])
    print ""

plt.errorbar(probdeer, mean_deer, yerr = sd_deer, fmt = '--o', c = 'blue', ecolor = 'black')
plt.ylim(0, np.max(mean_deer) + 200)
plt.title('Time Steps to Completion vs Prob of Deer Appearing (frac = 0.5, ncars = 10)')
plt.xlabel('Probability of Deer Appearing at any Place')
plt.ylabel('Time Steps until Completion')
plt.savefig('Time-vs-deerprob.png')
plt.show()
'''


#plt.ion()
#plt.clf()
#plt.draw()
#plt.pause()
